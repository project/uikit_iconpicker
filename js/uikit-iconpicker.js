/**
 * @file
 * Description.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.uikitIconpicker = {
    attach: function (context, settings) {
      // Define Uikit Icons
      var uikit_icon = {
        App: ['home', 'sign-in', 'sign-out', 'user', 'users', 'lock', 'unlock', 'settings', 'cog', 'nut', 'comment', 'commenting', 'comments', 'hashtag', 'tag', 'cart', 'bag', 'credit-card', 'mail', 'receiver', 'print', 'search', 'location', 'bookmark', 'code', 'paint-bucket', 'camera', 'video-camera', 'bell', 'microphone', 'bolt', 'star', 'heart', 'happy', 'lifesaver', 'rss', 'social', 'git-branch', 'git-fork', 'world', 'calendar', 'clock', 'history', 'future', 'crosshairs', 'pencil', 'trash', 'move', 'link', ' link-external', 'eye', 'eye-slash', 'question', 'info', 'warning', 'image', 'thumbnails', 'table', 'list', 'menu', 'grid', 'more', 'more-vertical', 'plus', 'plus-circle', 'minus', 'minus-circle', 'close', 'check', 'ban', 'refresh', 'play', 'play-circle'],
        Devices: ['tv', 'desktop', 'laptop', 'tablet', 'phone', 'tablet-landscape', 'phone-landscape'],
        Storage: ['file', 'file-text', 'file-pdf', 'copy', 'file-edit', 'folder', 'album', 'push', 'pull', 'server', 'database', 'cloud-upload', 'cloud-download', 'download', 'upload'],
        Direction: ['reply', 'forward', 'expand', 'shrink', 'arrow-up', 'arrow-down', 'arrow-left', 'arrow-right', 'chevron-up', 'chevron-down', 'chevron-left', 'chevron-right', 'chevron-double-left', 'chevron-double-right', 'triangle-up', 'triangle-down', 'triangle-left', 'triangle-right'],
        Editor: ['bold', 'italic', 'strikethrough', 'quote-right'],
        Brands: ['500px', 'android', 'android-robot', 'apple', 'behance', 'bluesky', 'discord', 'dribbble', 'etsy', 'facebook', 'flickr', 'foursquare', 'github', 'github-alt', 'gitter', 'google', 'instagram', 'joomla', 'linkedin', 'mastodon', 'microsoft', 'pinterest', 'reddit', 'signal', 'soundcloud', 'telegram', 'threads', 'tiktok', 'tripadvisor', 'tumblr', 'twitch', 'uikit', 'vimeo', 'whatsapp', 'wordpress', 'x', 'xing', 'yelp', 'youtube']
      };
      var elements = once('jsuikitIconpicker', '.uikit-iconpicker-element', context);
      $(elements).each(function () {
        $(this).fontIconPicker({
          source: uikit_icon,
          emptyIcon: true,
          hasSearch: true,
          useAttribute: true,
          attributeName: 'data-uk-icon',
          convertToHex: false
        });
      });
    }
  };

})(jQuery, Drupal);
