INTRODUCTION
------------

Integrates the UIkit 3 Icon with Drupal fields.
See : https://getuikit.com/docs/icon

REQUIREMENTS
------------

This module is an utility to manage an UIkit 3 icon Field.
It suppose you use the UIkit theme to load UIkit libraries
and icons both front-end and back-end's sides.
See : https://www.drupal.org/project/uikit

INSTALLATION
------------

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

CONFIGURATION
-------------

UIkit Iconpicker can be used on "Text (formatted)" or "Text (plain)" fields.
After attaching either of the above fields to an entity, you would be able to
use "UIkit Icon Picker" widget for those fields under *Manage form display*
section. You would also have to visit *Manage display* and configure the field
to use field formatter "UIkit Icon Picker" and adjust icon's ratio.
